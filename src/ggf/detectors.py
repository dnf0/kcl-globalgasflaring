from abc import ABC, abstractmethod
import pandas as pd
import numpy as np
from scipy.interpolate import RectBivariateSpline, interp1d, griddata
from scipy.ndimage import convolve, zoom
from scipy.interpolate.interpnd import _ndim_coords_from_arrays
from scipy.spatial import cKDTree
from skimage.morphology import binary_erosion, square
from skimage.filters import rank
from datetime import datetime

import src.config.constants as proc_const
from src.models import atsr_pixel_size
from src.models import slstr_pixel_size
from src.models import mwir_lut


class BaseDetector(ABC):

    def __init__(self,
                 day_night_angle=None,
                 swir_thresh=None,
                 cloud_window_size=None):

        """
        BaseDetector base class that contains all the shared attributes
        and methods used by the child classes that inherit from it.

        Args:
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
        """

        self.day_night_angle = day_night_angle
        self.swir_thresh = swir_thresh
        self.cloud_window_size = cloud_window_size
        self.sensor = None

        # TODO add inline descriptions to all attributes
        # setup attributes generated
        self.latitude = None
        self.longitude = None
        self.sza = None
        self.night_mask = None
        self.swir_16 = None
        self.swir_22 = None
        self.mwir_37 = None
        self.mwir_37_bt = None
        self.lwir_11_bt = None
        self.background_mwir_37 = None
        self.background_mask = None
        self.cloud_free = None
        self.cloudy = None
        self.local_cloudiness = None
        self.potential_hotspots_16 = None
        self.potential_hotspots_22 = None
        self.potential_hotspots_37 = None
        self.sensor = None
        self.pixel_size = None
        self.frp = None
        self.hotspots_16 = None
        self.hotspots_22 = None
        self.hotspots_37 = None
        self.aggregated_hotspots = None
        self.thresholds_to_test = None
        self.target_keys = None
        self.datetime_info = None

    def _make_night_mask(self) -> None:
        """
        Computes the day/night binary mask from
        the solar zenith angle.

        Returns:
            None
        """
        self.night_mask = self.sza >= self.day_night_angle

    def _compute_swir_frp(self) -> None:
        """
        Computes the pixel fire radiative power based on the
        Fisher and Wooster SWIR Radiance Method.
        https://doi.org/10.3390/rs10020305

        Returns:
            None
        """
        self.frp = self.pixel_size * proc_const.frp_coeff_swir_16[self.sensor] * self.swir_16 / 1000000  # in MW

    def _compute_local_cloudiness(self) -> None:
        """
        Computes the local mean cloudiness from binary cloud masks.

        Returns:
            None
        """
        k = np.ones([self.cloud_window_size, self.cloud_window_size])
        s = convolve(self.cloudy.astype(int), k, mode='constant', cval=0.0)
        count = convolve(np.ones(self.cloudy.shape), k, mode='constant', cval=0.0)
        self.local_cloudiness = s/count

    def _compute_background(self, image):

        buffer_size = 2
        background_size = 11

        # aggregate, mask and dilate hotspots
        aggregated_hotspots = np.zeros(self.swir_16.shape)
        for k in self.__dict__:
            if self.__dict__[k] is not None and "hotspots" in k and "potential" not in k:
                aggregated_hotspots += self.__dict__[k]
        aggregated_hotspots = aggregated_hotspots > 0
        hotspot_free = binary_erosion(~aggregated_hotspots, selem=square(buffer_size))  # note ~

        # dilate cloud mask
        cloud_free = binary_erosion(self.cloud_free, selem=square(buffer_size))

        # build background mask (cloud and hostpot free)
        background_mask = hotspot_free & cloud_free & self.night_mask

        # convert image to int and then estimate background from non hotspot pixels
        rescale = 1000
        int_image = (image * rescale).astype(np.uint16)
        background_image = rank.mean(int_image, selem=square(background_size), mask=background_mask)

        # if some backround data has no estimate then interpolate from nearby points
        if proc_const.interpolate_background:
            if (background_image == 0).any():
                # build interpolation arrays
                x = np.arange(0, self.swir_16.shape[1])
                y = np.arange(0, self.swir_16.shape[0])
                grid = np.meshgrid(x, y)

                invalid_mask = background_image == 0
                sampled = np.array([g[~invalid_mask] for g in grid]).T
                z = background_image[~invalid_mask]

                bg_image_fills = griddata(sampled, z, tuple(grid), 'linear')
                background_image[invalid_mask] = bg_image_fills[invalid_mask]

                # find distances from the nearest sample used during interpolation
                tree = cKDTree(sampled)
                xi = _ndim_coords_from_arrays(tuple(grid), ndim=sampled.shape[1])
                dists, indexes = tree.query(xi)

                # Limit interpolation which goes beyond the background window size.
                # TODO more thought is required on best way to do this.
                background_image[dists > background_size] = 0

        # find data points where sufficient background is available to provide a background estimate
        valid_background = rank.sum((background_image > 0).astype(np.uint8),
                                    selem=square(background_size)) / float(background_size ** 2)

        # convert data back to float and fill insufficient coverage
        background_image = background_image * 1.0 / rescale
        background_image[valid_background < proc_const.min_background_proportion] = proc_const.null_value

        return background_image

    def _build_dataframe(self, keys, sampling=False, joining_df=None) -> pd.DataFrame:
        """
        A flexible dataframe builder that takes in a set of keys that
        correspond to data contained within the object.  For each item
        of data, the samples associated with hotspot activity are selected
        using the provided mask.  The joining_df keyword allows the dataframe
        to be reduced as is sometimes required in the processing chain.

        Args:
            keys: The variables to be included in the dataframe (columns)
            sampling: Flag to determine if hotspot sampling is being evaluated
            joining_df: Used to reduce the dataframe through an Inner Join

        Returns:
            Dataframe containing the requested data defined by the input
            keys, flag, mask and, if included, the reducing dataframe.

        """
        df = pd.DataFrame()

        # store data associated with product
        for k in keys:
            if k not in self.__dict__:
                raise KeyError(k + ' not found in available attributes')
            if self.__dict__[k] is None:
                continue
            if sampling:
                df[k] = self.__dict__[k].flatten()  # Get everything then reduce in the join
            else:
                df[k] = self.__dict__[k][self.aggregated_hotspots]

        # store additional derived data
        if not sampling:
            lines, samples = np.where(self.aggregated_hotspots)
            df['line'] = lines
            df['sample'] = samples
        df['grid_x'] = self._find_arcmin_gridcell(df['latitude'])
        df['grid_y'] = self._find_arcmin_gridcell(df['longitude'])
        for time_period in self.datetime_info:
            df[time_period] = self.datetime_info[time_period]
        df['sensor'] = self.sensor

        if joining_df is not None:
            df = pd.merge(joining_df[['grid_x', 'grid_y']], df, on=['grid_x', 'grid_y'])
        return df

    def _detect_potential_hotspots(self, approach=None) -> None:

        if approach is None or approach not in ['swir_16_static',
                                                'swir_16_adaptive',
                                                'swir_22_adaptive',
                                                'mwir_37_adaptive',
                                                'nighttime_variable',
                                                ]:
            raise ValueError("Hotspot Detection approach not defined")

        if approach == 'swir_16_static':
            self.potential_hotspots_16 = self.swir_16 > self.swir_thresh
        if approach == 'swir_16_adaptive':
            self.potential_hotspots_16 = self.swir_16 > \
                                         np.mean(self.swir_16[self.swir_16 < proc_const.min_rad]) + \
                                         4 * np.std(self.swir_16[self.swir_16 < proc_const.min_rad])
        if approach == 'swir_22_adaptive':
            self.potential_hotspots_22 = self.swir_22 > \
                                         np.mean(self.swir_22[self.swir_22 < proc_const.min_rad]) + \
                                         4 * np.std(self.swir_22[self.swir_22 < proc_const.min_rad])
        if approach == 'mwir_37_adaptive':
            self.potential_hotspots_37 = (self.mwir_37_bt > proc_const.min_bt) & \
                                         (self.mwir_37_bt - self.lwir_11_bt > proc_const.min_bt_diff)
        if approach == 'nighttime_variable':
            for tvalue in self.thresholds_to_test:
                median = np.median(self.swir_16[self.swir_16 > tvalue])
                mean = np.mean(self.swir_16[self.swir_16 > tvalue])
                self.__dict__["potential_hotspots_16_" + str(tvalue)] = self.swir_16 > (tvalue + mean + 2 * median)

                # logging to console
                print("1.6 image mean:", np.mean(self.swir_16))
                print("1.6 image median:", np.median(self.swir_16))
                print("1.6 AT image mean:", mean)
                print("1.6 AT image median:", median)

                median = np.median(self.swir_22[self.swir_22 > tvalue])
                mean = np.mean(self.swir_22[self.swir_22 > tvalue])
                self.__dict__["potential_hotspots_22_" + str(tvalue)] = self.swir_22 > (tvalue + mean + 2 * median)

                # logging to console
                print("2.2 image mean:", np.mean(self.swir_22))
                print("2.2 image median:", np.median(self.swir_22))
                print("2.2 AT image mean:", mean)
                print("2.2 AT image median:", median)

    @staticmethod
    def _find_arcmin_gridcell(coordinates):
        """
        Ingests cartesian coordinates and rescales them
        to an integer representation of a 1-arminute grid.
        This is an approximate representation and is used
        solely for aggregation purposes (i.e. cannot be
        used for visualisation).

        Args:
            coordinates: A set of cartesian coordinates

        Returns:
            An integer value corresponding to an arcminute gridcell

        """
        neg_values = coordinates < 0

        abs_x = np.abs(coordinates)
        floor_x = np.floor(abs_x)
        decile = abs_x - floor_x
        minute = np.around(decile * 60)  # round to nearest arcmin
        minute_fraction = minute * 0.01  # convert to fractional value (ranges from 0 to 0.6)

        max_minute = minute_fraction > 0.59

        floor_x[neg_values] *= -1
        floor_x[neg_values] -= minute_fraction[neg_values]
        floor_x[~neg_values] += minute_fraction[~neg_values]

        # deal with edge cases - just round them all up
        if np.sum(max_minute) > 0:
            floor_x[max_minute] = np.around(floor_x[max_minute])

        # rescale
        floor_x = (floor_x * 100).astype(int)

        return floor_x

    @staticmethod
    def _rad_from_bt(wvl, b_temp):
        """
        Converts from brightness temperatures to spectral radiances
        Args:
            wvl: target wavelength
            b_temp: observed brightness temperature

        Returns:
            spectral radiances (W m-2 sr-1 um-1)
        """
        c1 = 1.19e-16  # W m-2 sr-1
        c2 = 1.44e-2  # mK
        wt = (wvl * 1.e-6) * b_temp  # m K
        d = (wvl * 1.e-6) ** 5 * (np.exp(c2 / wt) - 1)
        return c1 / d * 1.e-6  # W m-2 sr-1 um-1

    @abstractmethod
    def _load_arrays(self, subset=None) -> None:
        raise NotImplementedError("Must override _load_arrays")

    @abstractmethod
    def _extract_datetime(self) -> None:
        raise NotImplementedError("Must override _extract_datetime")

    @abstractmethod
    def run_detector(self, subset=None) -> None:
        raise NotImplementedError("Must override run_detector")

    @abstractmethod
    def to_dataframe(self, keys, joining_df=None, sampling=False) -> pd.DataFrame:
        raise NotImplementedError("Must override to_dataframe")


class ATXHotspotDetector(BaseDetector):

    def __init__(self,
                 product,
                 day_night_angle=proc_const.day_night_angle,
                 swir_thresh=proc_const.atx_swir_threshold,
                 cloud_window_size=proc_const.atx_cloud_window_size,
                 background_window_size=proc_const.atx_background_window_size):
        """
        Detector implementation for the Along Track Scanning Radiomter
        instruments (ATSR-1, ATSR-2, AATSR).
        Args:
            product: An ATSR product
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
            background_window_size: Image window over which local MWIR background statistics are computed
        """
        super().__init__(day_night_angle, swir_thresh, cloud_window_size)
        self.product = product
        self.background_window_size = background_window_size
        self._define_sensor()
        self._extract_datetime()

    def _extract_datetime(self) -> None:
        self.datetime_info = {'year': int(self.product.id_string[14:18]),
                              'month': int(self.product.id_string[18:20]),
                              'day': int(self.product.id_string[20:22]),
                              'hour': int(self.product.id_string[23:25]),
                              'minute': int(self.product.id_string[25:27])}

    def _define_sensor(self):
        """
        Identifies satellite type based on product id.
        Returns:
            Sensor code string

        """
        if 'ATS' in self.product.id_string:
            self.sensor = 'ats'
        if 'AT2' in self.product.id_string:
            self.sensor = 'at2'
        if 'AT1' in self.product.id_string:
            self.sensor = 'at1'

    def _load_arrays(self, subset=None) -> None:
        """
        Loads the product data needed for all processing.
        Args:
            subset: spatial subset range

        Returns:
            None
        """

        if subset is not None:
            h = subset['max_y'] - subset['min_y']
            yoffset = subset['min_y']
            w = subset['max_x'] - subset['min_x']
            xoffset = subset['min_x']
        else:
            h = None
            w = None
            yoffset = 0
            xoffset = 0

        self.latitude = self.product.get_band('latitude').read_as_array(width=w,
                                                                        xoffset=xoffset,
                                                                        height=h,
                                                                        yoffset=yoffset)
        self.longitude = self.product.get_band('longitude').read_as_array(width=w,
                                                                          xoffset=xoffset,
                                                                          height=h,
                                                                          yoffset=yoffset)
        self.cloud_free = self.product.get_band('cloud_flags_nadir').read_as_array(width=w,
                                                                                   xoffset=xoffset,
                                                                                   height=h,
                                                                                   yoffset=yoffset) <= 1
        self.pixel_size = np.tile(atsr_pixel_size.compute(), (self.cloud_free.shape[0], 1)) * 1000000  # km^2 to m^2

        swir_reflectance = self.product.get_band('reflec_nadir_1600').read_as_array(width=w,
                                                                                    xoffset=xoffset,
                                                                                    height=h,
                                                                                    yoffset=yoffset)
        self.swir_16 = np.nan_to_num(self._rad_from_ref(swir_reflectance))  # set nan's to zero

        mwir_brightness_temp = self.product.get_band('btemp_nadir_0370').read_as_array(width=w,
                                                                                       xoffset=xoffset,
                                                                                       height=h,
                                                                                       yoffset=yoffset)
        mwir_rad = self._rad_from_bt(3.7, mwir_brightness_temp)
        mwir_rad[mwir_rad < 0] = 0
        self.mwir_37 = mwir_rad

        solar_elev_angle_rad = np.deg2rad(self.product.get_band('sun_elev_nadir').read_as_array(width=w,
                                                                                                xoffset=xoffset,
                                                                                                height=h,
                                                                                                yoffset=yoffset))
        self.sza = np.rad2deg(np.arccos(np.sin(solar_elev_angle_rad)))

    def _rad_from_ref(self, reflectances):
        """
        Converts from 1.6 micron SWIR reflectances to spectral radiances
        Args:
            reflectances: 1.6 micron reflectances

        Returns:
            1.6 micron spectral radiances (W m-2 sr-1 um-1)

        """
        # convert from reflectance to radiance see Smith and Cox 2013
        se_dist = self._compute_sun_earth_distance() ** 2 / np.pi
        return reflectances / 100.0 * proc_const.solar_irradiance[self.sensor] * se_dist

    def _compute_sun_earth_distance(self):
        """
        Computes the Sun Earth distance based on the sensor date

        Returns:
            The Sun Earth distance at the time of observation

        """
        doy = datetime.strptime(self.product.id_string[14:22], "%Y%m%d").timetuple().tm_yday
        return 1 + 0.01672 * np.sin(2 * np.pi * (doy - 93.5) / 365.0)

    def run_detector(self, subset=None) -> None:
        """
        Runs the detector methods on the input data.

        Args:
            subset: specified if processing a subset of the array

        Returns:
            None
        """
        self._load_arrays(subset=subset)
        self._make_night_mask()
        self._detect_potential_hotspots(approach='swir_16_static')
        self.hotspots_16 = self.potential_hotspots_16 & self.night_mask
        self.aggregated_hotspots = self.hotspots_16

    def to_dataframe(self, keys, joining_df=None, sampling=False) -> pd.DataFrame:
        """
        Used to return a dataframe containing all needed information
        on the hotspots detected during the run_detector call.
        Args:
            keys: The data required in the dataframe
            joining_df: An joining dataframe that is used to reduce the hotspots.
            sampling: Flag to determine if hotspot sampling is being evaluated

        Returns:
            A dataframe containing the requested data.


        """
        if not ('latitude' in keys and 'longitude' in keys):
            raise KeyError('At a minimum, latitude and longitude are required')
        return self._build_dataframe(keys)


class ATXPersistentHotspotDetector(ATXHotspotDetector):

    def __init__(self,
                 product,
                 day_night_angle=proc_const.day_night_angle,
                 swir_thresh=proc_const.atx_swir_threshold,
                 cloud_window_size=proc_const.atx_cloud_window_size,
                 background_window_size=proc_const.atx_background_window_size):
        """
        Detector implementation for the Along Track Scanning Radiomter
        instruments (ATSR-1, ATSR-2, AATSR). Used to extract persistent hotspots and hotspot
        location sampling.
        Args:
            product: An ATSR product
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
            background_window_size: Image window over which local MWIR background statistics are computed
        """
        super().__init__(product, day_night_angle, swir_thresh, cloud_window_size, background_window_size)

    def run_detector(self, subset=None) -> None:
        """
        Runs the detector methods on the input data. Runs additional
        pixel characterisation over the parent class.

        Args:
            subset: specified if processing a subset of the array

        Returns:
            None
        """
        super().run_detector(subset=subset)
        self._compute_swir_frp()
        self.cloudy = ~self.potential_hotspots_16 & ~self.cloud_free & self.night_mask
        self._compute_local_cloudiness()
        self.background_mwir_37 = self._compute_background(self.mwir_37)

    def to_dataframe(self,
                     keys,
                     joining_df=None,
                     sampling=False):
        """
        Used to return a dataframe containing all needed information
        on presistent hotspots detected during the run_detector call.
        The information returned is dependent on the keys provided,
        and is screened using the joining_df keyword arg.
        Args:
            keys: The data required in the dataframe
            joining_df: An joining dataframe that can is used to reduce the hotspots.
            sampling: Flag to determine if hotspot sampling is being evaluated

        Returns:
            A dataframe containing the requested and reduced data.

        """
        if not ('latitude' in keys and 'longitude' in keys):
            raise KeyError('At a minimum, latitude and longitude keys are required')
        if joining_df is None:
            raise AttributeError('A joining_df must be provided for data reduction')
        return self._build_dataframe(keys, sampling=sampling, joining_df=joining_df)


class SLSHotspotDetector(BaseDetector):

    def __init__(self,
                 product,
                 day_night_angle=proc_const.day_night_angle,
                 swir_thresh=proc_const.sls_swir_threshold,
                 cloud_window_size=proc_const.sls_cloud_window_size):
        """
        Detector implementation for the Sea and Land Surface Temperature Scanning (SLSTR)
        radiometer instrument series.
        Args:
            product: SLSTR data product
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
        """
        super().__init__(day_night_angle, swir_thresh, cloud_window_size)
        self.product = product
        self.max_view_angle = proc_const.sls_vza_threshold  # degrees
        self.sensor = 'sls'
        self._extract_datetime()

    def _extract_datetime(self) -> None:
        dt_info = pd.Timestamp(self.product['time_an'].start_time)
        self.datetime_info = {'year': dt_info.year,
                              'month': dt_info.month,
                              'day': dt_info.day,
                              'hour': dt_info.hour,
                              'minute': dt_info.minute}

    def _load_arrays(self, subset=None) -> None:
        """
        Loads the product data needed for all processing.
        Returns:
            None
        """
        if subset is not None:
            min_y = subset['min_y']
            max_y = subset['max_y']
            min_x = subset['min_x']
            max_x = subset['max_x']
            self.latitude = self.product['geodetic_an']['latitude_an'][min_y:max_y, min_x:max_x]
            self.longitude = self.product['geodetic_an']['longitude_an'][min_y:max_y, min_x:max_x]
            self.swir_16 = self.product['S5_radiance_an']['S5_radiance_an'][min_y:max_y, min_x:max_x].filled(0)
            self.swir_22 = self.product['S6_radiance_an']['S6_radiance_an'][min_y:max_y, min_x:max_x].filled(0)
            self.sza = self._interpolate_array('solar_zenith_tn')[min_y:max_y, min_x:max_x].filled(0)
            self.vza = self._interpolate_array('sat_zenith_tn')[min_y:max_y, min_x:max_x].filled(9999)
            self.cloud_free = self.product['flags_an']['cloud_an'][min_y:max_y, min_x:max_x] == 0
            self.pixel_size = np.tile(np.array(slstr_pixel_size.pixel_size)[min_y:max_y],
                                      (self.vza.shape[0], 1)) * 1000000
        else:
            self.latitude = self.product['geodetic_an']['latitude_an'][:]
            self.longitude = self.product['geodetic_an']['longitude_an'][:]
            self.swir_16 = self.product['S5_radiance_an']['S5_radiance_an'][:].filled(0)
            self.swir_22 = self.product['S6_radiance_an']['S6_radiance_an'][:].filled(0)
            self.sza = self._interpolate_array('solar_zenith_tn').filled(0)
            self.vza = self._interpolate_array('sat_zenith_tn').filled(9999)
            self.cloud_free = self.product['flags_an']['cloud_an'][:] == 0
            self.pixel_size = np.tile(np.array(slstr_pixel_size.pixel_size), (self.vza.shape[0], 1)) * 1000000

    def _interpolate_array(self, target) -> np.array:
        """
        Interpolates SLSTR data arrays based on cartesian information
        contained within the sensor product using the RectBivariateSpline
        approach.

        Args:
            target: the product to be interpolated

        Returns:
            The interpolated data
        """
        sat_zn = self.product['geometry_tn'][target][:]

        tx_x_var = self.product['cartesian_tx']['x_tx'][0, :]
        tx_y_var = self.product['cartesian_tx']['y_tx'][:, 0]

        an_x_var = self.product['cartesian_an']['x_an'][:]
        an_y_var = self.product['cartesian_an']['y_an'][:]

        spl = RectBivariateSpline(tx_y_var, tx_x_var[::-1], sat_zn[:, ::-1].filled(0))
        interpolated = spl.ev(an_y_var.compressed(),
                              an_x_var.compressed())
        interpolated = np.ma.masked_invalid(interpolated, copy=False)
        sat = np.ma.empty(an_y_var.shape, dtype=sat_zn.dtype)
        sat[np.logical_not(np.ma.getmaskarray(an_y_var))] = interpolated
        sat.mask = an_y_var.mask
        return sat

    def _make_view_angle_mask(self):
        """
        Screen SLSTR data based on viewing zenith angle so
        that the data is limited to the max view angle (e.g.
        it can be set to 22 degrees to create an ATSR like product).

        Returns:
            None
        """
        self.vza_mask = self.vza <= self.max_view_angle

    def run_detector(self, subset=None) -> None:
        """
        Runs the detector methods on the input data.

        Args:
            subset: specified if processing a subset of the array

        Returns:
            None
        """
        self._load_arrays(subset=subset)
        self._make_night_mask()
        self._make_view_angle_mask()
        self._detect_potential_hotspots(approach='swir_16_static')
        self.hotspots_16 = self.potential_hotspots_16 & self.night_mask & self.vza_mask
        self.aggregated_hotspots = self.hotspots_16

    def to_dataframe(self,
                     keys,
                     joining_df=None,
                     sampling=False
                     ) -> pd.DataFrame:
        """
        Used to return a dataframe containing all needed information
        on the hotspots detected during the run_detector call.  The information
        returned is dependent on the keys provided, and if needed can be screened
        using the joining_df keyword arg.
        Args:
            keys: The data required in the dataframe
            joining_df: An joining dataframe that can is used to reduce the hotspots.
            sampling: Flag to determine if hotspot sampling is being evaluated

        Returns:
            A dataframe containing the requested data.

        """
        if not ('latitude' in keys and 'longitude' in keys):
            raise KeyError('At a minimum, latitude and longitude keys are required')
        return self._build_dataframe(keys)


class SLSPersistentHotspotDetector(SLSHotspotDetector):

    def __init__(self,
                 product,
                 day_night_angle=proc_const.day_night_angle,
                 swir_thresh=proc_const.sls_swir_threshold,
                 cloud_window_size=proc_const.sls_cloud_window_size):
        """
        Detector implementation for the Sea and Land Surface Temperature Scanning (SLSTR)
        radiometer instrument series.  Used to extract persistent hotspots and hotspot
        location sampling.
        Args:
            product: SLSTR data product
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
        """
        super().__init__(product, day_night_angle, swir_thresh, cloud_window_size)

    def run_detector(self, subset=None) -> None:
        """
        Runs the detector methods on the input data. Runs additional
        pixel characterisation over the parent class.

        Args:
            subset: specified if processing a subset of the array

        Returns:
            None
        """
        super().run_detector(subset=subset)
        self._compute_swir_frp()
        self.cloudy = ~self.potential_hotspots_16 & ~self.cloud_free & self.night_mask & self.vza_mask
        self._compute_local_cloudiness()

    def to_dataframe(self,
                     keys,
                     joining_df=None,
                     sampling=False):
        """
        Used to return a dataframe containing all needed information
        on presistent hotspots detected during the run_detector call.
        The information returned is dependent on the keys provided,
        and is screened using the joining_df keyword arg.
        Args:
            keys: The data required in the dataframe
            joining_df: An joining dataframe that can is used to reduce the hotspots.
            sampling: Flag to determine if hotspot sampling is being evaluated

        Returns:
            A dataframe containing the requested and reduced data.

        """
        if not ('latitude' in keys and 'longitude' in keys):
            raise KeyError('At a minimum, latitude and longitude keys are required')
        if joining_df is None:
            raise AttributeError('A joining_df must be provided for data reduction')
        return self._build_dataframe(keys, sampling=sampling, joining_df=joining_df)


class SLSHotspotPhaseDetector(SLSHotspotDetector):
    def __init__(self,
                 product,
                 day_night_angle=proc_const.day_night_angle,
                 swir_thresh=proc_const.sls_swir_threshold,
                 cloud_window_size=proc_const.sls_cloud_window_size):
        """
        Generates all needed data for estimating fire phase from SWIR
        and MWIR nighttime SLSTR data.
        Args:
            product: SLSTR data product
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
        """
        super().__init__(product, day_night_angle, swir_thresh, cloud_window_size)

    @staticmethod
    def _zoom_array(arr) -> np.ndarray:
        """
        Rescales the SLSTR thermal infrared images
        so that they have the same resolution as the
        SWIR channels.  Nearest Neighbour interpolation used

        Args:
            arr: SLSTR array to be zoomed

        Returns:
            A rescaled image array

        """
        factor = 2  # magnification factor
        return zoom(arr, factor, order=0)

    @staticmethod
    def _bt_to_rad(arr) -> np.ndarray:
        """
        Converts brightness temperature array
        into spectral radiance array using ESA
        provided LUT.
        Args:
            arr: an array of brightness temperatures

        Returns:
            an array of temperatures

        """
        lut = interp1d(mwir_lut.temps, mwir_lut.rads)

        nulls = arr < 0
        arr[nulls] = 300
        arr_rad = lut(arr)
        arr_rad[nulls] = np.nan

        return arr_rad

    def _load_arrays(self, subset=None) -> None:
        """
        Loads the product data needed for all processing.
        Returns:
            None
        """
        super()._load_arrays(subset=subset)
        if subset is not None:

            min_y = subset['min_y']
            max_y = subset['max_y']
            min_x = subset['min_x']
            max_x = subset['max_x']

            # magnify arrays to same dims and swir data
            self.mwir_37_bt = self._zoom_array(self.product['S7_BT_in']['S7_BT_in'][min_y:max_y, min_x:max_x])
            self.lwir_11_bt = self._zoom_array(self.product['S8_BT_in']['S8_BT_in'][min_y:max_y, min_x:max_x])

            # generate radiances
            self.mwir_37 = self._bt_to_rad(self.mwir_37_bt)
        else:
            # magnify arrays to same dims and swir data
            self.mwir_37_bt = self._zoom_array(self.product['S7_BT_in']['S7_BT_in'][:])
            self.lwir_11_bt = self._zoom_array(self.product['S8_BT_in']['S8_BT_in'][:])

            # generate radiances
            self.mwir_37 = self._bt_to_rad(self.mwir_37_bt)

    def run_detector(self, subset=None) -> None:
        """
        Runs the detector methods on the input data. Runs additional
        pixel characterisation over the parent class.

        Args:
            subset: specified if processing a subset of the array

        Returns:
            None
        """
        self._load_arrays(subset=subset)
        self._make_night_mask()
        for approach in ['swir_16_adaptive', 'swir_22_adaptive', 'mwir_37_adaptive']:
            self._detect_potential_hotspots(approach=approach)
        self.hotspots_16 = self.potential_hotspots_16 & self.night_mask
        self.hotspots_22 = self.potential_hotspots_22 & self.night_mask
        self.hotspots_37 = self.potential_hotspots_37 & self.night_mask & self.cloud_free
        self.aggregated_hotspots = self.hotspots_16 | self.hotspots_22 | self.hotspots_37
        self.background_mwir_37 = self._compute_background(self.mwir_37)


class SLSThresholdComparisonDetector(SLSHotspotDetector):
    def __init__(self,
                 product,
                 thresholds,
                 day_night_angle=proc_const.day_night_angle,
                 swir_thresh=proc_const.sls_swir_threshold,
                 cloud_window_size=proc_const.sls_cloud_window_size):
        """
        Used to compare different SLSTR hotspot detection thresholds
        Args:
            product: SLSTR data product
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
        """
        super().__init__(product, day_night_angle, swir_thresh, cloud_window_size)
        self.thresholds_to_test = thresholds
        self.target_keys = []

    def run_detector(self, subset=None) -> None:
        self._load_arrays(subset=subset)
        self._make_night_mask()
        self._detect_potential_hotspots(approach='nighttime_variable')
        store = {}
        for k in self.__dict__:
            if self.__dict__[k] is not None and 'potential_hotspots' in k:
                target = k.strip('potential_hotspots')
                new_key = 'hotspots_' + target
                store[new_key] = self.__dict__[k] & self.night_mask
                self.target_keys.append(new_key)
        for k in store:
            self.__dict__[k] = store[k]

        # when writing to csv is should have consistent number of columns
        # so aggregate the hotspots to ensure that this is the case.
        self.aggregated_hotspots = np.array(list(store.values())).sum(axis=0) > 0

    def to_dataframe(self,
                     keys,
                     joining_df=None,
                     sampling=False
                     ) -> pd.DataFrame:

        # override keys
        other_keys = ['latitude', 'longitude', 'swir_16', 'swir_22']
        [self.target_keys.append(other_key) for other_key in other_keys]
        return self._build_dataframe(self.target_keys, sampling=sampling, joining_df=joining_df)
