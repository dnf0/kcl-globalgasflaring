import os
import sys
import pandas as pd
import numpy as np

import src.config.filepaths as fp


def load_csvs(path_to_process, keys, cols=None) -> pd.DataFrame:
    """
    Generate a dataframe from a set of CSV files retaining
    specified columns.

    Args:
        path_to_process: Path holding csv files
        keys: filenames to be included in the processing
        cols: Columns to use (all if None)

    Returns:
        Pandas dataframe generated from the input CSV files
    """
    df_container = []
    for root, dirs, files in os.walk(path_to_process, topdown=True, followlinks=True):
        for f in files:
            if 'threshold_evaluation' in f:
                try:
                    p = os.path.join(root, f)
                    df = pd.read_csv(p, usecols=cols)
                    at_least_one_true = df[keys].sum(axis=1) > 0
                    df = df[at_least_one_true]
                    df_container.append(df)
                except pd.errors.EmptyDataError:
                    continue
    return pd.concat(df_container, ignore_index=True, copy=False)


def main():

    sensor = sys.argv[1]
    year = sys.argv[2]
    month = sys.argv[3]
    if sensor not in ['atx', 'sls']:
        raise KeyError("Sensor not in" + "['atx', 'sls']")

    keys = ['hotspots_16_0.03', 'hotspots_22_0.03',
            'hotspots_16_0.05', 'hotspots_22_0.05',
            'hotspots_16_0.1', 'hotspots_22_0.1',
            'hotspots_16_0.15', 'hotspots_22_0.15',
            'hotspots_16_0.2', 'hotspots_22_0.2']

    # set paths and target columns
    if sensor == 'atx':
        path_to_process = os.path.join(fp.output_l2, sensor, str(year), str(month).zfill(2))
    else:
        path_to_process = os.path.join(fp.output_l2, sensor, str(year), str(month).zfill(2))
    df = load_csvs(path_to_process, keys)
    df['counter'] = 1

    # groupby cols
    to_group = ['line', 'sample', 'year', 'month', 'day', 'hour', 'minute']

    for k in keys:
        print("grouping", k)
        cols = [k] + to_group + ['grid_x', 'grid_y', 'counter']
        sub_df = df[cols]

        # extract detections group and save
        sub_df = sub_df[sub_df[k]]

        grouped_sub_df = sub_df.groupby(to_group).agg({'counter': np.sum, 'grid_x': 'first', 'grid_y': 'first'})
        outpath = os.path.join(fp.output_l3 + '/' + k + f"_counts_{sensor}_{month}.csv")
        grouped_sub_df.to_csv(outpath)


if __name__ == "__main__":
    main()
