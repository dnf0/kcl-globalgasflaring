import os
import sys
import tempfile
import subprocess

import src.config.filepaths as fp
from src.utils import build_outpath


def presubmission_checks(file_to_process,
                         sensor,
                         proc_flags):
    # if not reprocessing then check if files has already been processed.
    if not proc_flags['reprocess']:
        log_file = os.path.join(fp.logs, ".".join([proc_flags['stage'], 'txt']))
        if os.path.isfile(log_file):
            with open(os.path.join(fp.logs, '.'.join([proc_flags['stage'], 'txt'])), 'r') as f:
                if any(file_to_process == x.rstrip('\r\n') for x in f):
                    print(file_to_process, 'already processed')
                    return False
    # exclude SLSTR NRT data and just use NTC data
    if "_NR_" in file_to_process:
        return False
    return True


def submit(script, file_to_process, sensor):

    # build the various paths required
    base_path = build_outpath(sensor, fp.output_l2, file_to_process, None)
    (gd, temp_file) = tempfile.mkstemp('.sh', 'ggf.', base_path, True)
    g = os.fdopen(gd, "w")
    g.write('#!/bin/bash\n')
    g.write('export PYTHONPATH=$PYTHONPATH:/home/users/dnfisher/projects/kcl-globalgasflaring/\n')
    g.write(" ".join([fp.script_dir + script, file_to_process, sensor + "\n"]))
    g.write(" ".join(["rm -f ", temp_file + "\n"]))
    g.close()
    os.chmod(temp_file, 0o755)

    cmd = " ".join(['sbatch', '-p', 'short-serial',
                    '-o', os.path.join(base_path, '%j.out'),
                    '-e', os.path.join(base_path, '%j.err'),
                    temp_file])
    try:
        subprocess.call(cmd, shell=True)
    except Exception as e:
        print('Subprocess failed with error:', str(e))


def main():
    script = sys.argv[1]
    sensor = sys.argv[2]
    year = sys.argv[3]
    month = sys.argv[4]

    # check args
    if sensor not in ['ats', 'at2', 'at1', 'sls']:
        raise NotImplementedError(sensor)
    if script not in ['hotspots', 'flares', 'threshold_evaluation']:
        raise NotImplementedError(script)

    # set processing flags
    proc_flags = {'reprocess': False, 'stage': script}

    # append filetype to script
    script += '.py'

    # TODO reverse filepath order so that most recent files are processed first
    path_to_process = os.path.join(fp.products[sensor], str(year), str(month).zfill(2))
    for root, dirs, files in os.walk(path_to_process, topdown=True, followlinks=True):
        for f in files:
            if f.endswith(('.zip', 'N1', 'E2', 'E1')):
                file_to_process = os.path.join(root, f)
                if not presubmission_checks(file_to_process, sensor, proc_flags):
                    continue
                submit(script, file_to_process, sensor)


if __name__ == "__main__":
    main()
