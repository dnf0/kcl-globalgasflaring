#!/home/users/dnfisher/venvs/ggf/bin/python3

import os
import sys
import epr
import pandas as pd
import numpy as np

from src.ggf.detectors import ATXPersistentHotspotDetector, SLSPersistentHotspotDetector
import src.utils as utils
import src.config.filepaths as fp


def merge_hotspot_dataframes(atx_persistent_df, sls_persistent_df):
    atx_persistent_df['sensor'] = 1
    sls_persistent_df['sensor'] = -1
    appended_df = atx_persistent_df.append(sls_persistent_df)
    hotspot_df = appended_df.groupby(['lats_arcmin', 'lons_arcmin'], as_index=False).agg({'sensor': np.sum})

    # the below information is not used here but we can use this
    # idea to update the sensor information during the post processing
    hotspot_df.sensor.loc[hotspot_df.sensor == 1] = 'atx'
    hotspot_df.sensor.loc[hotspot_df.sensor == -1] = 'sls'
    hotspot_df.sensor.loc[hotspot_df.sensor == 0] = 'both'
    return hotspot_df


def aggregate(df, aggregator):
    return df.groupby(['grid_y', 'grid_x'], as_index=False).agg(aggregator)


def main():
    file_to_process = sys.argv[1]
    sensor = sys.argv[2]

    if sensor != 'sls':
        product = epr.Product(file_to_process)
        hotspot_detector = ATXPersistentHotspotDetector(product)

        flare_keys = ['latitude',
                      'longitude',
                      'local_cloudiness',
                      'swir_16',
                      'frp',
                      'pixel_size',
                      'mwir',
                      'background_mwir']

        flare_aggregator = {'frp': np.sum,
                            'swir_16': np.mean,
                            'mwir': np.mean,
                            'background_mwir': np.mean,
                            'pixel_size': np.sum,
                            'latitude': np.mean,
                            'longitude': np.mean,
                            'local_cloudiness': np.mean,
                            'year': 'first',
                            'month': 'first',
                            'day': 'first',
                            'hhmm': 'first'}

        sampling_keys = ['latitude',
                         'longitude',
                         'local_cloudiness']

        sampling_aggregator = {'local_cloudiness': np.mean,
                               'year': 'first',
                               'month':  'first',
                               'day': 'first',
                               'hhmm': 'first'}

        atx_persistent_fp = os.path.join(fp.output_l3,
                                         'all_sensors',
                                         'all_flare_locations_ats.csv')
        persistent_df = pd.read_csv(atx_persistent_fp)

    else:
        temp_path = utils.build_outpath(sensor, fp.slstr_extract_temp, file_to_process, None)
        product = utils.extract_zip(file_to_process, temp_path)
        hotspot_detector = SLSPersistentHotspotDetector(product)

        flare_keys = ['latitude',
                      'longitude',
                      'local_cloudiness',
                      'swir_16',
                      'swir_22',
                      'frp',
                      'pixel_size']

        flare_aggregator = {'frp': np.sum,
                            'swir_16': np.mean,
                            'swir_22': np.mean,
                            'pixel_size': np.sum,
                            'latitude': np.mean,
                            'longitude': np.mean,
                            'local_cloudiness': np.mean,
                            'year': 'first',
                            'month':  'first',
                            'day': 'first',
                            'hhmm': 'first'}

        sampling_keys = ['latitude',
                         'longitude',
                         'local_cloudiness',
                         ]

        sampling_aggregator = {'local_cloudiness': np.mean,
                               'year': 'first',
                               'month': 'first',
                               'day': 'first',
                               'hhmm': 'first'
                               }

        # merge persistent dataframes for SLSTR
        atx_persistent_fp = os.path.join(fp.output_l3,
                                         'all_sensors',
                                         'all_flare_locations_atx.csv')
        atx_persistent_df = pd.read_csv(atx_persistent_fp)

        sls_persistent_fp = os.path.join(fp.output_l3,
                                         'all_sensors',
                                         'all_flare_locations_sls.csv')
        sls_persistent_df = pd.read_csv(sls_persistent_fp)

        persistent_df = merge_hotspot_dataframes(atx_persistent_df,
                                                 sls_persistent_df)
    # find persistent hotspots (i.e. flares)
    hotspot_detector.run_detector()
    flare_df = hotspot_detector.to_dataframe(keys=flare_keys,
                                             joining_df=persistent_df)

    if not flare_df.empty:
        aggregated_flare_df = aggregate(flare_df, flare_aggregator)
        aggregated_flare_df.to_csv(utils.build_outpath(sensor, fp.output_l2, file_to_process, 'flares'))

    # record processing of file
    log_file = os.path.join(fp.logs, '.'.join(['flares', 'txt']))
    with open(log_file, 'a+') as f:
        f.write(file_to_process + '\n')

    # get sampling associated with persistent hotspots
    sampling_df = hotspot_detector.to_dataframe(keys=sampling_keys,
                                                sampling=True,
                                                joining_df=persistent_df)

    if not sampling_df.empty:
        aggregated_sampling_df = aggregate(sampling_df, sampling_aggregator)
        aggregated_sampling_df.to_csv(utils.build_outpath(sensor, fp.output_l2, file_to_process, 'sampling'))

    # record processing of file
    log_file = os.path.join(fp.logs, '.'.join(['sampling', 'txt']))
    with open(log_file, 'a+') as f:
        f.write(file_to_process + '\n')


if __name__ == "__main__":
    main()
