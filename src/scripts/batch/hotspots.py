#!/home/users/dnfisher/venvs/ggf/bin/python3
import sys
import os
import epr

from src.ggf.detectors import ATXHotspotDetector, SLSHotspotDetector
import src.utils as utils
import src.config.filepaths as fp


def main():
    file_to_process = sys.argv[1]
    sensor = sys.argv[2]

    if sensor != 'sls':
        product = epr.Product(file_to_process)
        hotspot_detector = ATXHotspotDetector(product)
        keys = ['latitude', 'longitude']
    else:
        temp_path = utils.build_outpath(sensor, fp.slstr_extract_temp, file_to_process, None)
        product = utils.extract_zip(file_to_process, temp_path)
        hotspot_detector = SLSHotspotDetector(product)
        keys = ['latitude', 'longitude']

    hotspot_detector.run_detector()
    df = hotspot_detector.to_dataframe(keys=keys)

    if not df.empty:
        df.to_csv(utils.build_outpath(sensor, fp.output_l2, file_to_process, 'hotspots'))

    # record processing of file
    log_file = os.path.join(fp.logs, '.'.join(['hotspots', 'txt']))
    with open(log_file, 'a+') as f:
        f.write(file_to_process + '\n')


if __name__ == "__main__":
    main()
