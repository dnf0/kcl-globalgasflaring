#!/home/users/dnfisher/venvs/ggf/bin/python3

import sys
import os

from src.ggf.detectors import SLSThresholdComparisonDetector
import src.utils as utils
import src.config.filepaths as fp


def main():
    file_to_process = sys.argv[1]
    sensor = sys.argv[2]

    if sensor != 'sls':
        raise ValueError('Not appropriate to run on non-SLSTR data')
    else:
        temp_path = utils.build_outpath(sensor, fp.slstr_extract_temp, file_to_process, None)
        product = utils.extract_zip(file_to_process, temp_path)
        thresholds_to_test = [0.03, 0.05, 0.1, 0.15, 0.2]
        hotspot_detector = SLSThresholdComparisonDetector(product, thresholds_to_test)
        keys = ['swir_16', 'swir_22', 'latitude', 'longitude',
                'hotspots_16_0.03', 'hotspots_22_0.03',
                'hotspots_16_0.05', 'hotspots_22_0.05',
                'hotspots_16_0.1', 'hotspots_22_0.1',
                'hotspots_16_0.15', 'hotspots_22_0.15',
                'hotspots_16_0.2', 'hotspots_22_0.2']

    hotspot_detector.run_detector()
    df = hotspot_detector.to_dataframe(keys)
    if not df.empty:
        df.to_csv(utils.build_outpath(sensor, fp.output_l2, file_to_process, 'threshold_evaluation'))

    # record processing of file
    log_file = os.path.join(fp.logs, '.'.join(['threshold_evaluation', 'txt']))
    with open(log_file, 'a+') as f:
        f.write(file_to_process + '\n')


if __name__ == "__main__":
    main()
