import os

import pytest
from netCDF4 import Dataset
from numpy.testing import assert_allclose, assert_equal
from numpy import load
from numpy import arange
import pandas as pd
import epr
import zipfile

from src.ggf import detectors
import src.config.constants as proc_const

FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    '..',
    'test_files',
    )

ATX_SUBSET = {'min_y': 7700,
              'max_y': 8200,
              'min_x': 0,
              'max_x': 512}


@pytest.fixture()
def atx_product_data():
    raw_data_path = os.path.join(FIXTURE_DIR, "atx_raw")
    for f in os.listdir(raw_data_path):
        with zipfile.ZipFile(os.path.join(raw_data_path, f)) as product_file:
            product_file.extract(product_file.filelist[0], raw_data_path)
            product_fname = product_file.filelist[0].filename
        product = epr.Product(os.path.join(raw_data_path, product_fname))
        os.remove(os.path.join(raw_data_path, product_fname))
    return product


@pytest.fixture()
def atx_target_data():
    targets = {}
    target_data_path = os.path.join(FIXTURE_DIR, "atx_target")
    for f in os.listdir(target_data_path):
        source = load(os.path.join(target_data_path, f), allow_pickle=True)
        var_name = str(f).split('/')[-1].split('.')[0]
        targets[var_name] = source['arr_0']
    return targets


@pytest.fixture()
def atx_dataframe_data():
    dataframes = {}
    target_data_path = os.path.join(FIXTURE_DIR, "atx_dataframes")
    for f in os.listdir(target_data_path):
        source = pd.read_csv(os.path.join(target_data_path, f), index_col=[0])
        var_name = str(f).split('/')[-1].split('.')[0]
        dataframes[var_name] = source
    return dataframes


@pytest.fixture
def sls_product_data():
    product = {}
    raw_data_path = os.path.join(FIXTURE_DIR, "sls_raw")
    for f in os.listdir(raw_data_path):
        source = Dataset(os.path.join(raw_data_path, f))
        var_name = str(f).split('/')[-1].split('.')[0]
        product[var_name] = source
    return product


@pytest.fixture()
def sls_target_data():
    targets = {}
    target_data_path = os.path.join(FIXTURE_DIR, "sls_target")
    for f in os.listdir(target_data_path):
        source = load(os.path.join(target_data_path, f))
        var_name = str(f).split('/')[-1].split('.')[0]
        targets[var_name] = source['arr_0']
    return targets


@pytest.fixture()
def sls_dataframe_data():
    dataframes = {}
    target_data_path = os.path.join(FIXTURE_DIR, "sls_dataframes")
    for f in os.listdir(target_data_path):
        source = pd.read_csv(os.path.join(target_data_path, f), index_col=[0])
        var_name = str(f).split('/')[-1].split('.')[0]
        dataframes[var_name] = source
    return dataframes


def test_atx_constructor(atx_product_data):
    atx_detector = detectors.ATXHotspotDetector(atx_product_data)
    assert atx_detector.sensor == 'ats'
    assert atx_detector.background_window_size == proc_const.atx_background_window_size
    target_datetime_info = {'year': 2010, 'month': 11, 'day': 20, 'hour': 15, 'minute': 34}
    values_match = all([target_datetime_info[k] == atx_detector.datetime_info[k]
                        for k in atx_detector.datetime_info.keys()])
    assert values_match


def test_atx_run_detector_hotspots(atx_product_data, atx_target_data):
    atx_detector = detectors.ATXHotspotDetector(atx_product_data)
    atx_detector.run_detector(subset=ATX_SUBSET)

    h = ATX_SUBSET['max_y'] - ATX_SUBSET['min_y']
    yoffset = ATX_SUBSET['min_y']
    assert_allclose(atx_detector.latitude, atx_product_data.get_band('latitude').read_as_array(height=h,
                                                                                               yoffset=yoffset))
    assert_allclose(atx_detector.longitude, atx_product_data.get_band('longitude').read_as_array(height=h,
                                                                                                 yoffset=yoffset))
    assert_allclose(atx_detector.sza, atx_target_data['sza'], rtol=1e-05)
    assert_allclose(atx_detector.swir_16, atx_target_data['swir_16'], rtol=1e-05)
    assert_allclose(atx_detector.mwir_37, atx_target_data['mwir_37'], rtol=1e-05)
    assert_allclose(atx_detector.pixel_size, atx_target_data['pixel_size'], rtol=1e-05)
    assert_equal(atx_detector.night_mask, atx_target_data['night_mask'])
    assert_equal(atx_detector.cloud_free, atx_target_data['cloud_free'])
    assert_equal(atx_detector.potential_hotspots_16, atx_target_data['potential_hotspots_16'])
    assert_equal(atx_detector.hotspots_16, atx_target_data['hotspots_16'])


def test_atx_run_detector_persistent_hotspots(atx_product_data, atx_target_data):
    atx_detector = detectors.ATXPersistentHotspotDetector(atx_product_data)
    atx_detector.run_detector(subset=ATX_SUBSET)

    assert_allclose(atx_detector.frp, atx_target_data['frp'])
    assert_allclose(atx_detector.local_cloudiness, atx_target_data['local_cloudiness'])
    assert_allclose(atx_detector.background_mwir_37, atx_target_data['background_mwir_37'])
    assert_equal(atx_detector.background_mask, atx_target_data['background_mask'])
    assert_equal(atx_detector.cloudy, atx_target_data['cloudy'])


def est_atx_to_dataframe_hotspots(atx_product_data, atx_dataframe_data):
    atx_detector = detectors.ATXHotspotDetector(atx_product_data)
    atx_detector.run_detector(subset=ATX_SUBSET)

    # test minimum key error raised
    with pytest.raises(KeyError):
        atx_detector.to_dataframe(['latitude'])

    # test hotspot detection
    keys = ['latitude', 'longitude']
    hotspots_df = atx_detector.to_dataframe(keys)
    assert_allclose(hotspots_df[keys].values, atx_dataframe_data['hotspots'][keys].values)


def test_atx_to_dataframe(atx_product_data, atx_dataframe_data):
    atx_detector = detectors.ATXPersistentHotspotDetector(atx_product_data)
    atx_detector.run_detector(subset=ATX_SUBSET)

    # test flare detection
    flare_keys = ['latitude',
                  'longitude',
                  'local_cloudiness',
                  'swir_16',
                  'frp',
                  'pixel_size',
                  'mwir_37',
                  'background_mwir_37']
    flare_df = atx_detector.to_dataframe(keys=flare_keys,
                                         joining_df=atx_dataframe_data['joining'])
    assert_allclose(flare_df[flare_keys].values, atx_dataframe_data['flares'][flare_keys].values)

    # test sampling detection
    sampling_keys = ['latitude',
                     'longitude',
                     'local_cloudiness']

    # sampling also requires a joining_df
    with pytest.raises(AttributeError):
        atx_detector.to_dataframe(keys=sampling_keys,
                                  sampling=True)

    sampling_df = atx_detector.to_dataframe(keys=sampling_keys,
                                            sampling=True,
                                            joining_df=atx_dataframe_data['joining'])
    assert_allclose(sampling_df[sampling_keys].values, atx_dataframe_data['samples'][sampling_keys].values)


def test_sls_constructor(sls_product_data):
    sls_detector = detectors.SLSHotspotDetector(sls_product_data)
    assert sls_detector.sensor == 'sls'
    keys_match = all([k1 == k2 for k1, k2 in zip(sls_detector.product.keys(), sls_product_data.keys())])
    assert keys_match
    assert sls_detector.max_view_angle == proc_const.sls_vza_threshold

    target_datetime_info = {'year': 2020, 'month': 2, 'day': 22, 'hour': 20, 'minute': 31}
    values_match = all([target_datetime_info[k] == sls_detector.datetime_info[k]
                        for k in sls_detector.datetime_info.keys()])
    assert values_match


def test_sls_run_detector_hotspots(sls_product_data, sls_target_data):
    sls_detector = detectors.SLSHotspotDetector(sls_product_data)
    sls_detector.run_detector()

    # basic input data
    assert_allclose(sls_detector.swir_16, sls_product_data['S5_radiance_an']['S5_radiance_an'][:].filled(0))
    assert_allclose(sls_detector.swir_22, sls_product_data['S6_radiance_an']['S6_radiance_an'][:].filled(0))
    assert_allclose(sls_detector.latitude, sls_product_data['geodetic_an']['latitude_an'][:])
    assert_allclose(sls_detector.longitude, sls_product_data['geodetic_an']['longitude_an'][:])

    # other data
    assert_allclose(sls_detector.sza, sls_target_data['sza'])
    assert_allclose(sls_detector.vza, sls_target_data['vza'])
    assert_equal(sls_detector.vza_mask, sls_target_data['vza_mask'])
    assert_equal(sls_detector.night_mask, sls_target_data['night_mask'])
    assert_equal(sls_detector.cloud_free, sls_target_data['cloud_free'])
    assert_equal(sls_detector.potential_hotspots_16, sls_target_data['potential_hotspots_16'])
    assert_equal(sls_detector.hotspots_16, sls_target_data['hotspots_16'])


def test_sls_run_detector_persistent_hotspots(sls_product_data, sls_target_data):
    sls_detector = detectors.SLSPersistentHotspotDetector(sls_product_data)
    sls_detector.run_detector()

    assert_allclose(sls_detector.frp, sls_target_data['frp'])
    assert_allclose(sls_detector.local_cloudiness, sls_target_data['local_cloudiness'])
    assert_equal(sls_detector.cloudy, sls_target_data['cloudy'])


def test_sls_to_dataframe_hotspots(sls_product_data, sls_dataframe_data):
    sls_detector = detectors.SLSHotspotDetector(sls_product_data)
    sls_detector.run_detector()

    # test no key error raised
    with pytest.raises(KeyError):
        sls_detector.to_dataframe(['latitude'])

    # test hotspot detection
    keys = ['latitude', 'longitude']
    hotspots_df = sls_detector.to_dataframe(keys)
    assert_allclose(hotspots_df[keys].values, sls_dataframe_data['hotspots'][keys].values)


def test_sls_to_dataframe_persistent_hotspots(sls_product_data, sls_dataframe_data):
    sls_detector = detectors.SLSPersistentHotspotDetector(sls_product_data)
    sls_detector.run_detector()

    # test flare detection
    flare_keys = ['latitude',
                  'longitude',
                  'local_cloudiness',
                  'swir_16',
                  'frp',
                  'pixel_size']
    flare_df = sls_detector.to_dataframe(flare_keys,
                                         joining_df=sls_dataframe_data['joining'])
    assert_allclose(flare_df[flare_keys].values, sls_dataframe_data['flares'][flare_keys].values)

    # test sampling detection
    sampling_keys = ['latitude',
                     'longitude',
                     'local_cloudiness']

    # sampling also requires a joining_df
    with pytest.raises(AttributeError):
        sls_detector.to_dataframe(sampling_keys,
                                  sampling=True)

    sampling_df = sls_detector.to_dataframe(sampling_keys,
                                            sampling=True,
                                            joining_df=sls_dataframe_data['joining'])
    assert_allclose(sampling_df[sampling_keys].values, sls_dataframe_data['samples'][sampling_keys].values)


def test_sls_to_dataframe_hotspot_phase(sls_product_data, sls_dataframe_data):
    sls_detector = detectors.SLSHotspotPhaseDetector(sls_product_data)
    sls_detector.run_detector()

    float_keys = ['latitude',
                  'longitude',
                  'pixel_size',
                  'swir_16',
                  'mwir_37',
                  'background_mwir_37']
    bool_keys = ['hotspots_16',
                 'hotspots_22',
                 'hotspots_37']
    all_keys = float_keys + bool_keys

    phase_df = sls_detector.to_dataframe(all_keys)
    assert_allclose(phase_df[float_keys].values, sls_dataframe_data['phase'][float_keys].values)
    assert_equal(phase_df[bool_keys].values, sls_dataframe_data['phase'][bool_keys].values)


def test_sls_to_dataframe_threshold_comparison(sls_product_data, sls_dataframe_data):
    thresholds_to_test = arange(0.01, 0.06, 0.01)
    sls_detector = detectors.SLSThresholdComparisonDetector(sls_product_data, thresholds_to_test)
    sls_detector.run_detector()
    float_keys = ['swir_16', 'swir_22', 'latitude', 'longitude']
    bool_keys = ['hotspots_16_0.01', 'hotspots_22_0.01', 'hotspots_16_0.02',
                 'hotspots_22_0.02', 'hotspots_16_0.03', 'hotspots_22_0.03',
                 'hotspots_16_0.04', 'hotspots_22_0.04', 'hotspots_16_0.05',
                 'hotspots_22_0.05']
    all_keys = float_keys + bool_keys

    threshold_comparison_df = sls_detector.to_dataframe(all_keys)
    assert_allclose(threshold_comparison_df[float_keys].values,
                    sls_dataframe_data['variable_threshold'][float_keys].values)
    assert_equal(threshold_comparison_df[bool_keys].values, sls_dataframe_data['variable_threshold'][bool_keys].values)
